# VRChat World Bookmark Manager #

![MainWindow](https://bitbucket.org/tcmeta/world-bookmark-manager/raw/master/build/bitbucket/MainPage.png)

This is a Desktop application that allows users to view and manage their recently visited worlds, allow to connect to them via the ui etc. Now with friendlist!

### Features ###

* Grab the worlds you goto as you explore vrchat and put them into a easy to view history.
* Single click access to worlds you have accessed.
* Easy search function.
* Api world id resolution (need account details).
* Friends list that updates faster than in game.
* Able to access worlds within VRChat

### Getting Started ###

This app has been built with [Electron](https://electron.atom.io/) and can be run with [Node.js](https://nodejs.org/) or run from a packaged version found in the [downloads section](https://bitbucket.org/tcmeta/world-bookmark-manager/downloads/). You can also visit [here](https://vrchat.com/launch?worldId=wrld_89b812aa-f48f-4667-af9c-d9bf524f2cc4&ref=vrchat.com) and if this does not exist anymore. You can download the world from in the [downloads section](https://bitbucket.org/tcmeta/world-bookmark-manager/downloads/).

### Who do I talk to? ###

* [Vargink](https://keybase.io/theguywho)