/*
    Vargink's VRChat world bookmark thingy

    

*/
const electron = require('electron');
const {app, Tray, Menu, BrowserWindow, ipcMain, shell} = require('electron');
const url = require('url');
const path = require('path');
const storage = require('electron-json-storage');
const Tail = require('tail').Tail;
const request = require('request');

const express = require('express');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer();
const webserver = express();
const port = 3542;

webserver.use( bodyParser.json() );
webserver.use(bodyParser.urlencoded({extended: true})); 

const {autoUpdater} = require("electron-updater");


let mainWindow = null;
let appIcon = null;
const iconPath = path.join(__dirname, '/app/img/VRChatBookmark.png');

var main = {};
main.settings = {};
main.worldHistory = {};
main.worldList = {};
main.userList = {};
isDebug = true;

var saveListTimer = null;
var saveHistTimer = null;
var saveUserTimer = null;
var worldApiTimer = null;
var logTailer = null;

var friendApiTimer = null;

var friendList = {
    error: false,
    online: []
};
// Debug

function debug(message) {
    if (isDebug) {
        console.log(message);
    }
}
// make the app quit if we have it running already

var shouldQuit = app.makeSingleInstance(function(commandLine, workingDirectory) {
    // Someone tried to run a second instance, we should focus our window.
    if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore();
        mainWindow.focus();
    }
});
  
if (shouldQuit) {
    app.quit();
    return;
}

webserver.get('/', function(request, response) {
    if (request.headers.origin) {
        response.header('Access-Control-Allow-Origin', '*')
        response.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization')
        response.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
        if (request.method === 'OPTIONS') return response.send(200)
    }
    response.send('VRChat Helper');
});

webserver.post('/', upload.array(), function(request, response) {
    if (request.headers.origin) {
        response.header('Access-Control-Allow-Origin', '*')
        response.header('Access-Control-Allow-Headers', 'X-Requested-With,Content-Type,Authorization')
        response.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE')
        if (request.method === 'OPTIONS') return response.send(200)
    }
    
    try {
        let returnList = [];
        let worldDump = [];
        switch(request.body.cmd) {
            case 'historyList':
                if (request.body.index == -1) {
                    request.body.index = Math.floor(main.worldHistory.id.length / request.body.count);
                    if (request.body.index == main.worldHistory.id.length / request.body.count) {
                        --request.body.index;
                    }
                }
                if (request.body.index > main.worldHistory.id.length / request.body.count) {
                    request.body.index = 0;
                }
                for (var i = Number(request.body.index) * Number(request.body.count); i < Number(request.body.index) * Number(request.body.count) + Number(request.body.count) && i < main.worldHistory.id.length; i++) {
                    let tempWorldName = main.worldList[main.worldHistory.id[i].worldID].name;
                    if (tempWorldName == '') {
                        tempWorldName = main.worldHistory.id[i].worldID;
                    }
                    returnList.push({
                        worldID: main.worldHistory.id[i].worldID,
                        worldName: tempWorldName,
                        thumbnailImageUrl: main.worldList[main.worldHistory.id[i].worldID].thumbnailImageUrl,
                        favourite: main.worldList[main.worldHistory.id[i].worldID].favourite
                    });
                    
                }
                response.json({cmd: 'historyList', value: returnList, index: request.body.index, count: request.body.count});
                break;
            case 'favList':
                for (i in main.worldList) {
                    if (i != 'version'  && main.worldList[i].favourite) {
                        worldDump.push({
                            worldID: i,
                            worldName: main.worldList[i].name,
                            thumbnailImageUrl: main.worldList[i].thumbnailImageUrl,
                            favourite: main.worldList[i].favourite
                        });
                    }
                }
                for (var i = Number(request.body.index) * Number(request.body.count); i < Number(request.body.index) * Number(request.body.count) + Number(request.body.count) && i < worldDump.length; i++) {
                    if (worldDump[i].worldName == '') {
                        worldDump[i].worldName = worldDump[i].worldID;
                    }
                    returnList.push({
                        worldID: worldDump[i].worldID,
                        worldName: worldDump[i].worldName,
                        thumbnailImageUrl:worldDump[i].thumbnailImageUrl,
                        favourite: worldDump[i].favourite
                    });
                }
                response.json({cmd: 'favList', value: returnList, index: request.body.index, count: request.body.count});
                break;
            case 'worldList':
                for (i in main.worldList) {
                    if (i != 'version') {
                        worldDump.push({
                            worldID: i,
                            worldName: main.worldList[i].name,
                            thumbnailImageUrl: main.worldList[i].thumbnailImageUrl,
                            favourite: main.worldList[i].favourite
                        });
                    }
                }
                if (request.body.index == -1) {
                    request.body.index = Math.floor(worldDump.length / request.body.count);
                    if (request.body.index == worldDump.length / request.body.count) {
                        --request.body.index;
                    }
                }
                if (request.body.index > worldDump.length / request.body.count) {
                    request.body.index = 0;
                }
                for (var j = Number(request.body.index) * Number(request.body.count); j < Number(request.body.index) * Number(request.body.count) + Number(request.body.count) && j < worldDump.length; j++) {
                    if (worldDump[j].worldName == '') {
                        worldDump[j].worldName = worldDump[j].worldID;
                    }
                    returnList.push({
                        worldID: worldDump[j].worldID,
                        worldName: worldDump[j].worldName,
                        thumbnailImageUrl:worldDump[j].thumbnailImageUrl,
                        favourite: worldDump[j].favourite
                    });
                }
                response.json({cmd: 'worldList', value: returnList, index: request.body.index, count: request.body.count});
                break;
        }
    } catch (e) {
        response.send({
            err: 'something went wrong'+e.message
        });
    }
});

ipcMain.on('debug', function(event, message) { 
    debug(message);
});

ipcMain.on('main', function(event, data) { 
    switch (data.cmd) {
        case 'save':
            save(data.value);
            break;
        case 'startWatchLog':
            startWatchLog();
            break;
        case 'getApi':
            getApiKey(data.value);
            break;
        case 'checkUpdate':
            autoUpdater.checkForUpdates();
            break;
        case 'installUpdate':
            autoUpdater.quitAndInstall();
            break;
        case 'historyList':
            var returnList = main.worldHistory.id;
            for (var i = 0; i < returnList.length; i++) {
                returnList[i].worldName = main.worldList[returnList[i].worldID].name;
                returnList[i].thumbnailImageUrl = main.worldList[returnList[i].worldID].thumbnailImageUrl;
                returnList[i].favourite = main.worldList[returnList[i].worldID].favourite;
                if (returnList[i].worldName == '') {
                    returnList[i].worldName = returnList[i].worldID;
                }
            }
            mainWindow.webContents.send('mainWindow', {cmd: 'buildList', value: returnList, button: data.cmd});
            break;
        case 'searchList':
            var returnList = [];
            debug(data.value);
            for (i in main.worldList) {
                if (i != 'version') {
                    if (main.worldList[i].name.toLowerCase().indexOf(data.value.toLowerCase()) > -1) {
                        returnList.push({
                            worldID: i,
                            worldName: main.worldList[i].name,
                            thumbnailImageUrl: main.worldList[i].thumbnailImageUrl,
                            favourite: main.worldList[i].favourite
                        });
                    }
                }
            }
            mainWindow.webContents.send('mainWindow', {cmd: 'buildList', value: returnList});
            break;
        case 'worldList':
            var returnList = [];
            for (i in main.worldList) {
                if (i != 'version') {
                    returnList.push({
                        worldID: i,
                        worldName: main.worldList[i].name,
                        thumbnailImageUrl: main.worldList[i].thumbnailImageUrl,
                        favourite: main.worldList[i].favourite
                    });
                }
            }
            mainWindow.webContents.send('mainWindow', {cmd: 'buildList', value: returnList});
            break;
        case 'favList':
            var returnList = [];
            for (i in main.worldList) {
                if (i != 'version'  && main.worldList[i].favourite) {
                    returnList.push({
                        worldID: i,
                        worldName: main.worldList[i].name,
                        thumbnailImageUrl: main.worldList[i].thumbnailImageUrl,
                        favourite: main.worldList[i].favourite
                    });
                }
            }
            mainWindow.webContents.send('mainWindow', {cmd: 'buildList', value: returnList});
            break;
        case 'friendList':
            // lets put your favourite people in front of everyone else 
            var favList = [];
            var restList = [];
            for (var i = 0; i < friendList.online.length; i++) {
                friendList.online[i].favourite = main.userList[friendList.online[i].id].favourite;
                if (main.userList[friendList.online[i].id].favourite == true){
                    favList.push(friendList.online[i]);
                } else {
                    favList.push(friendList.online[i]);
                }
            }
            favList.concat(restList);
            mainWindow.webContents.send('mainWindow', {cmd: 'friendList', value: favList});
            break;
        case 'worldProperty':
            var returnList = main.worldList[data.value];
            returnList.worldID = data.value;
            returnList.mainList = 'worldList';
            mainWindow.webContents.send('mainWindow', {cmd: 'propertyWindow', value: returnList});
            break;
        case 'userProperty':
            var returnList = {};
            returnList.worldID = data.value;
            for (var i = 0; i < friendList.online.length; i++) {
                if (friendList.online[i].id == data.value) {
                    returnList.thumbnailImageUrl = friendList.online[i].currentAvatarThumbnailImageUrl;
                    returnList.name = friendList.online[i].displayName;
                    returnList.favourite = main.userList[friendList.online[i].id].favourite;
                    returnList.description = '';
                    returnList.authorName = '';
                    returnList.mainList = 'userList';
                    break;
                }
            }
            mainWindow.webContents.send('mainWindow', {cmd: 'propertyWindow', value: returnList});
            break;
        case 'favToggle':
            main[data.value.list][data.value.id].favourite = !main[data.value.list][data.value.id].favourite;
            save(data.value.list);
            mainWindow.webContents.send('mainWindow', {cmd: 'favToggle', value: {list: data.value.list, id: data.value.id, favourite: main[data.value.list][data.value.id].favourite}});
            break;
    };
});

function save(objectName) {
    storage.set(objectName, main[objectName], function(error) {
        if (error) {
            debug(error);
            return false;
        } else {
            return true;
        }
    });
}

function startWatchLog() {
    // start tailing logs.
    debug('Start log tailing');
    stopWatchLog();
    sendUI('Starting VRChat Logtail!');
    logTailer = new Tail(main.settings.logLocation);

    logTailer.on('line', function(data) {
        if (data.indexOf('Joining wrld_') > -1) {
            var worldID = data.substr(data.indexOf('wrld_'),41);
            var timestamp = data.substring(data.indexOf('[Log:')+5,data.indexOf(']'));
            debug('found world: '+worldID,41);
            if (!main.worldList.hasOwnProperty(worldID)) {
                main.worldList[worldID] = {name: '', description: '', authorName: '', thumbnailImageUrl: '', favourite: false};
                // this could dump really hard so lets just make a timeout to allow for everything to work itself out before we commit to saving it locally.
                clearTimeout(saveListTimer);
                saveListTimer = setTimeout(function() {
                    save('worldList');
                }, 1000);
                clearTimeout(worldApiTimer);
                worldApiTimer = setTimeout(function() {
                    worldListUpdate();
                }, 1000);
            }
            // add worlds to history
            main.worldHistory.id.push({
                id: main.worldHistory.id.length,
                worldID: worldID,
                date: timestamp
            });
            // maybe we should organise them in the array in date order.?
            main.worldHistory.id.sort(function(a,b){
                return new Date(b.date) - new Date(a.date);
            });
            // save to json timeout so we dont spam rewrites.
            clearTimeout(saveHistTimer);
            saveHistTimer = setTimeout(function() {
                save('worldHistory');
                // send messag to the main window to make it update the history list.
                mainWindow.webContents.send('mainWindow', {cmd: 'updateWorldList'});
            }, 1000);
        }
    });
    logTailer.on('error', function(error) {
        debug('ERROR: ', error);
        sendUI('Error with VRChat Log Parser');
        stopWatchLog();
    });
}

function stopWatchLog() {
    if (logTailer !== null) {
        logTailer.unwatch()
        logTailer = null;
    }
}

function sendUI(message) {
    mainWindow.webContents.send('ui-msg', message);
}

function getApiKey(userHash) {
    sendUI('Getting API Key');
    request({
        url: main.settings.apiURL+'/config',
        headers: {
            Authorization : 'Basic '+main.settings.userHash
        }
    }, function(error, response, body) {
        if (error) {
            sendUI('Something went wrong with getting your API Key!');
            debug('http error: '+error);
        } else if (response.statusCode === 200) {
            debug('http hit!');
            try {
                body = JSON.parse(body);
                debug('api: '+body.clientApiKey);
                main.settings.userHash = userHash;
                main.settings.apiKey = body.clientApiKey;
                save('settings');
                sendUI('Got your api key! Kicking in resolver and friends list!');
                worldListUpdate();
                getOnlineUsers();
            } catch (e) {
                debug('error'+e.message);
                sendUI('Something went wrong with getting your API Key!');
            }
        } else {
            debug('error nfi: '+body+' : '+response);
            sendUI('Something went wrong with getting your API Key!');
        }
    });
}

function getOnlineUsers() {
    if (main.settings.userHash !== null && main.settings.apiKey !== null) {
        request({
            url: main.settings.apiURL+'/auth/user/friends?apiKey='+main.settings.apiKey+'&?offline=false',
            headers: {
                Authorization : 'Basic '+main.settings.userHash
            }
        }, function(error, response, body) {
            if (error) {
                debug('http error: '+error);
                friendList.error = error;
            } else if (response.statusCode === 200) {
                debug('http hit!');
                try {
                    friendList.online = JSON.parse(body);
                    // lets check for new users and add some values to them.
                    for (var i = 0; i < friendList.online.length; i++) {
                        if (!main.userList.hasOwnProperty(friendList.online[i].id)) {
                            main.userList[friendList.online[i].id] = {nickname: null, favourite: false};
                        }
                        clearTimeout(saveUserTimer);
                        saveUserTimer = setTimeout(function() {
                            save('userList');
                        }, 1000);
                    }
                    friendList.online.sort(function(a, b){
                        if(a.displayName < b.displayName) return -1;
                        if(a.displayName > b.displayName) return 1;
                        return 0;
                    });
                    friendList.error = false;
                    // kick in the timer to keep checking this.
                    mainWindow.webContents.send('mainWindow', {cmd: 'listUpdated', value: 'friendList'});
                    if (friendApiTimer === null) {
                        friendApiTimer = setInterval(function() {
                            getOnlineUsers();
                        }, 30000);
                    }
                } catch (e) {
                    debug('error'+e.message);
                    friendList.error = e.message;
                }
            } else {
                debug('error nfi: '+body+' : '+response);
                friendList.error = body;
            }
            if (friendApiTimer !== null && friendList.error !== false) {
                clearInterval(friendApiTimer);
                friendApiTimer = null;
            }
        });
    } else if (friendApiTimer !== null) {
        clearInterval(friendApiTimer);
        friendApiTimer = null;
    }
}

function worldListUpdate() {
    if (main.settings.userHash !== null && main.settings.apiKey !== null) {
        for (var i in main.worldList) {
            // go through till we find an empty world name
            if(main.worldList[i].name == '') {
                debug('Geting info for world ID: '+i);
                // we havent looked at this one.
                request({
                    url: main.settings.apiURL+'/worlds/'+i+'?apiKey='+main.settings.apiKey,
                    headers: {
                        Authorization : 'Basic '+main.settings.userHash
                    }
                }, function(error, response, body) {
                    if (error) {
                        debug('http error: '+error);
                    } else if (response.statusCode === 200) {
                        debug('http hit!');
                        try {
                            body = JSON.parse(body);
                            main.worldList[i].name = body.name;
                            main.worldList[i].description = body.description;
                            main.worldList[i].authorName = body.authorName;
                            main.worldList[i].thumbnailImageUrl = body.thumbnailImageUrl;
                            save('worldList');
                            mainWindow.webContents.send('mainWindow', {cmd: 'updateWorldID', value: main.worldList[i]});
                            // kick in the next one and wait a few seconds before we try hitting the api again (lets not be agressive for the moment)
                            clearTimeout(worldApiTimer);
                            worldApiTimer = setTimeout(function() {
                                worldListUpdate();
                            }, 1000);
                            debug('World info updated:');
                            debug('name: '+main.worldList[i].name+' description: '+main.worldList[i].description+' author: '+main.worldList[i].authorName+' thumbnail: '+main.worldList[i].thumbnailImageUrl);
                        } catch (e) {
                            debug('error'+e.message);
                        }
                    } else {
                        debug('error nfi: '+body+' : '+response);
                    }
                });
                break;
            }
        }
    }
}

function windowToggle() {
    if (mainWindow.isVisible()) {
        mainWindow.hide();
    } else {
        mainWindow.show();
    }
}
// update stuff.
autoUpdater.on('checking-for-update', function() {
    sendUI('Checking for update...');
});
autoUpdater.on('update-available', function(ev, info) {
    sendUI('Update available.');
});
autoUpdater.on('update-not-available', function(ev, info) {
    sendUI('You are up to date.');
    mainWindow.webContents.send('mainWindow', {cmd: 'noUpdate'});
});
autoUpdater.on('error', function(ev, err) {
    sendUI('Error in auto-updater.');
});
autoUpdater.on('download-progress', function(ev, progressObj) {
    sendUI('Download progress...');
});
autoUpdater.on('update-downloaded', function(ev, info) {
    sendUI('Update downloaded! Goto settings to install it');
    mainWindow.webContents.send('mainWindow', {cmd: 'yesUpdate'});
});


// application start function
app.on('ready',function() { 
    // load settings
    storage.get('settings', function(error, data) {
        if (JSON.stringify(data) === JSON.stringify({}) || error) {
            // no file exists
            main.settings = {
                version: 1,
                logLocation: path.normalize(app.getPath('appData')+'/../LocalLow/VRChat/vrchat;output_log.txt'), // should be where VRChat keeps its logs by default.
                apiURL: 'https://api.vrchat.cloud/api/1',
                apiKey: null,
                userHash: null
            };
            save('settings');
        } else {
            main.settings = data;
        }
        storage.get('worldHistory', function(error, data) {
            if (JSON.stringify(data) === JSON.stringify({}) || error) {
                // no file exists
                main.worldHistory = {
                    version: 1,
                    id: []
                };
                save('worldHistory');
            } else {
                main.worldHistory = data;
            }
            storage.get('worldList', function(error, data) {
                if (JSON.stringify(data) === JSON.stringify({}) || error) {
                    // no file exists
                    main.worldList = {
                        version: 1
                    };
                    save('worldList');
                } else {
                    main.worldList = data;
                    debug('worldhistory loaded'+JSON.stringify(main.worldList, 0,2));
                }
                storage.get('userList', function(error, data) {
                    if (JSON.stringify(data) === JSON.stringify({}) || error) {
                        // no file exists
                        main.userList = {
                            version: 1
                        };
                        save('userList');
                    } else {
                        main.userList = data;
                    }
                    // any updates to settings here
                    if (main.worldList.version == 1) {
                        for (var i in main.worldList) {
                            if (i != 'version') {
                                main.worldList[i].favourite = false;
                            }
                        }
                        main.worldList.version = 2;
                        save('worldList');
                    }
                    debug('starting webserver');
                    // Web Server here
                    webserver.listen(port, function(err) {
                        if (err) {
                          return debug('something bad happened', err)
                        }
                        debug(`server is listening on ${port}`)
                    });

                    debug('starting UI');
                    // Add UI Stuff here
                    mainWindow = new BrowserWindow({
                        width: 650,
                        height: 600,
                        show: true,
                        icon: iconPath,
                        frame: false
                    });
                    mainWindow.loadURL(url.format({
                        pathname: path.join(__dirname, '/app/app.html'),
                        protocol: 'file:',
                        slashes: true
                    }));
                    appIcon = new Tray(iconPath);
                    var contextMenu = Menu.buildFromTemplate([
                        {
                            label: 'Open',
                            click: function() {
                                windowToggle();
                            }
                        },
                        {
                            label: 'Start VRChat',
                            click: function() {
                                shell.openExternal('steam://rungameid/438100');
                            }
                        },
                        {
                            label: 'Exit',
                            click: function() {
                                app.quit();
                            }
                        }
                    ]);
                    appIcon.on('click', function(e) {
                        windowToggle();
                    });
                    appIcon.setToolTip("VRChat Helper");
                    appIcon.setContextMenu(contextMenu);
                    // Make world API fucntion run because hell why not.
                    debug('kicking in the tail');
                    startWatchLog();
                    debug('Kicking in the update API');
                    worldListUpdate();
                    debug('kicking in the friends list');
                    getOnlineUsers();
                    //check for updates
                    autoUpdater.checkForUpdates();
                });
            });
        });
    });
});
