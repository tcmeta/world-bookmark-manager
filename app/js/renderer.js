const {remote, ipcRenderer, shell} = require('electron');
const {dialog, app} = require('electron').remote;
const iconPath = './img/people-i.png';
const path = require('path');

function debug(message) {
    if (remote.getGlobal('isDebug')) {
        ipcRenderer.send('debug', message);
    }
}

function iconGreen(buttonName) {
    $('#worldList').css('color', 'white');
    $('#favList').css('color', 'white');
    $('#historyList').css('color', 'white');
    $('#friendList').css('color', 'white');
    $('#searchList').css('color', 'white');
    $('#'+buttonName).css('color', 'green');
}

function uiMsg(message) {
    $('.ui-text').text(message);
    $('.ui-msg').animate({
        "top":"30px"
    }, 1000);
    $('.body').animate({
        "top":"60px"
    }, 1000);
    $('.settings').animate({
        "top":"60px"
    }, 1000);
    $('.properties').animate({
        "top":"60px"
    }, 1000);
}

function start() {
    debug('Starting App');
    // when initing lets generate the list
    ipcRenderer.send('main', {cmd: 'historyList'});
    iconGreen('historyList');
    ipcRenderer.on('ui-msg', function(event, message) {
        uiMsg(message);
    });
    ipcRenderer.on('mainWindow', function(event, data) {
        switch (data.cmd) {
            case 'listUpdated':
                debug('hit updater: '+data.value);
                if ($('#'+data.value).css("color") == "rgb(0, 128, 0)") {
                    debug('sending updater: '+data.value);
                    ipcRenderer.send('main', {cmd: data.value});
                }
                break;
            case 'updateWorldID':
                $('div[worldID="'+data.value.worldID+'"]').html('<div class="left"><img src="'+data.value.thumbnailImageUrl+'"></div><div class="middle">'+data.value.name+'</div><div class="right button fontawesome" id="gotoWorld" gotoID="'+data.value.worldID+'"><i class="fas fa-door-open"></i></div>');
                break;
            case 'updateWorldList':
                listBuild();
                break;
            case 'noUpdate':
                $('#installUpdate').hide();
                break;
            case 'yesUpdate':
                $('#installUpdate').show();
                break;
            case 'friendList':
                debug('friend build');
                var timeStamp = event.timeStamp;
                var topOffset = 0;
                for (var i = 0; i < data.value.length; i++) {
                    if ($('div[listIndex="'+data.value[i].id+'"]').length == 0) {
                        $('.body').append('<div class="world-item button" id="user" timeStamp="'+timeStamp+'" listIndex="'+data.value[i].id+'" worldID="'+data.value[i].location+'"><div class="left">'+( data.value[i].favourite  == true ?  '<div class=fav><i class="fas fa-star"></i></div>' : '')+'<img src="'+data.value[i].currentAvatarThumbnailImageUrl+'"></div><div class="middle">'+data.value[i].displayName+'</div><div class="right button fontawesome" id="gotoWorld" gotoID="'+data.value[i].location+'"><i class="fas fa-door-open"></i></div>');
                    } else {
                        $('.div[listIndex="'+data.value[i].id+'"]').attr('timeStamp', timeStamp);
                        $('div[listIndex="'+data.value[i].id+'"]').html('<div class="left">'+( data.value[i].favourite  == true ?  '<div class=fav><i class="fas fa-star"></i></div>' : '')+'<img src="'+data.value[i].currentAvatarThumbnailImageUrl+'"></div><div class="middle">'+data.value[i].displayName+'</div><div class="right button fontawesome" id="gotoWorld" gotoID="'+data.value[i].location+'"><i class="fas fa-door-open"></i>')
                    }
                    $('div[listIndex="'+data.value[i].id+'"]').animate({
                        "top": topOffset+"px"
                    }, 200);
                    topOffset += 40;
                }
                $('.world-item').each(function( index ){
                     if ($(this).attr('timeStamp') < timeStamp) {
                        $(this).remove();
                    }
                });
                break;
            case 'buildList':
                var topOffset = 0;
                for (var i = 0; i < data.value.length; i++) {
                    if ($('div[listIndex="'+i+'"]').length == 0) {
                        $('.body').append('<div class="world-item button" id="world" listIndex="'+i+'" worldID="'+data.value[i].worldID+'"><div class="left">'+( data.value[i].favourite  == true ?  '<div class=fav><i class="fas fa-star"></i></div>' : '')+'<img src="'+data.value[i].thumbnailImageUrl+'"></div><div class="middle">'+data.value[i].worldName+'</div><div class="right button fontawesome" id="gotoWorld" gotoID="'+data.value[i].worldID+'"><i class="fas fa-door-open"></i></div>');
                    }
                    $('div[listIndex="'+i+'"]').animate({
                        "top": topOffset+"px"
                    }, 200);
                    topOffset += 40;
                }
                break;
            case 'propertyWindow':
                $('.propertyImage').html('<img src="'+data.value.thumbnailImageUrl+'">');
                $('.propertyName').html(data.value.name);
                if (data.value.favourite) {
                    $('.propertyFav').css('color', 'yellow');
                } else {
                    $('.propertyFav').css('color', 'black');
                }
                $('.propertyID').html(data.value.worldID);
                $('.propertyFav').attr('worldID', data.value.worldID);
                $('.propertyFav').attr('mainList', data.value.mainList);
                $('.propertyDesc').html(data.value.description);
                $('.propertyAuthor').html(data.value.authorName);
                $('.properties').animate({"left":"0"}, 1000);
                break;
            case 'favToggle':
                // just set the heart thats currently there to the value returned
                if (data.value.favourite) {
                    $('.propertyFav').css('color', 'yellow');
                } else {
                    $('.propertyFav').css('color', 'black');
                }
                break;
        };
    });
    $(document).on('dblclick', '.header', function() {
        if (remote.BrowserWindow.getFocusedWindow().isMaximized) {
            remote.BrowserWindow.getFocusedWindow().restore();
        } else {
            remote.BrowserWindow.getFocusedWindow().maximize();
        }
    });
    $(document).on('keypress', '#search', function(e) {
        if (e.keyCode == 13) {
            $('.body').html('');
            ipcRenderer.send('main', {cmd: 'searchList', value: $('#search').val()});
        }
    });
    $(document).on('click', '.button', function() {
        debug('button click');
        switch (this.id) {
            case 'exit':
                remote.BrowserWindow.getFocusedWindow().hide();
                 $('.settings').animate({"left":"100%"}, 0);
                 $('.properties').animate({"left":"100%"}, 0);
                break;
            case 'settings':
                if ($('.settings').css('left') !== "0px") {
                    $('.settings').animate({"left":"0"}, 1000);
                } else {
                    $('.settings').animate({"left":"100%"}, 1000);
                }
                break;
            case 'properties':
                if ($('.properties').css('left') !== "0px") {
                    $('.properties').animate({"left":"0"}, 1000);
                } else {
                    $('.properties').animate({"left":"100%"}, 1000);
                }
            break;
            case 'ui-hide': 
                $('.ui-msg').animate({
                    "top":"0px"
                }, 1000);
                $('.body').animate({
                    "top":"30px"
                }, 1000);
                $('.settings').animate({
                    "top":"30px"
                }, 1000);
                $('.properties').animate({
                    "top":"30px"
                }, 1000);
                break;
            case 'gotoWorld':
                shell.openExternal('vrchat://launch?ref=vrchat.com&id='+$(this).attr('gotoID'));
                break;
            case 'logSelect':
                var fileSelect = dialog.showOpenDialog({defaultPath: path.normalize(app.getPath('appData')+'/../LocalLow'), properties: ['openFile']});
                if (fileSelect.length != 0) {
                    // got a file
                    remote.getGlobal('settings').logLocation = fileSelect[0];
                    ipcRenderer.send('main', {cmd: 'save', value: 'settings'});
                    ipcRenderer.send('main', {cmd: 'startWatchLog'});
                }
                break;
            case 'login':
                ipcRenderer.send('main', {cmd: 'getApi', value: new Buffer($('#username').val()+':'+$('#password').val()).toString('base64')});
                break;
            case 'checkUpdate':
                ipcRenderer.send('main', {cmd: 'checkUpdate'});
                break;
            case 'historyList':
                iconGreen('historyList');
                $('.body').html('');
                ipcRenderer.send('main', {cmd: 'historyList'});
                break;
            case 'worldList':
                iconGreen('worldList');
                $('.body').html('');
                ipcRenderer.send('main', {cmd: 'worldList'});
                break;
            case 'favList':
                iconGreen('favList');
                $('.body').html('');
                ipcRenderer.send('main', {cmd: 'favList'});
                break;
            case 'searchList':
                $('.body').html('');
                ipcRenderer.send('main', {cmd: 'searchList', value: $('#search').val()});
                break;
            case 'friendList':
                iconGreen('friendList');
                $('.body').html('');
                ipcRenderer.send('main', {cmd: 'friendList'});
                break;
            case 'world':
                ipcRenderer.send('main', {cmd: 'worldProperty', value: $(this).attr('worldID')});
                break;
            case 'favToggle':
                ipcRenderer.send('main', {cmd: 'favToggle', value: {list: $(this).attr('mainList'), id: $(this).attr('worldID')}});
                break;
            case 'user':
                ipcRenderer.send('main', {cmd: 'userProperty', value: $(this).attr('listIndex')});
                break;
            case 'installUpdate':
                ipcRenderer.send('main', {cmd: 'installUpdate'});
                break;
        }
    });
}

$( document ).ready(function() {
    start();
});